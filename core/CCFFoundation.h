
#ifndef CCFOUNDATION_H__
#define CCFOUNDATION_H__

#ifdef __OBJC__

#import "assertions.h"
#import "CCFUnicodeUtilities.h"
#import "macros.h"
#import "rand.h"
#import "time.h"

#import "NSObject+CCFExtensions.h"
#import "NSArray+CCFExtensions.h"
#import "NSData+CCFExtensions.h"
#import "NSDate+CCFExtensions.h"
#import "NSDictionary+CCFExtensions.h"
#import "NSMutableDictionary+CCFExtensions.h"
#import "NSMutableString+CCFExtensions.h"
#import "NSString+CCFExtensions.h"
#import "NSString+CCFConversions.h"
#import "NSXMLNode+CCFExtensions.h"
#import "NSCalendar+CCFExtensions.h"

#endif



#endif
