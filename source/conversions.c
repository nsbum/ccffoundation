
#include <stdio.h>
#include <math.h>

#include "conversions.h"

static const float INCHES_TO_CENTIMETERS = 2.54;
static const float POUNDS_TO_KILOGRAMS = 0.4535924;

#pragma mark - Metric to US

float cm_to_inches(float cm)
{
    return cm / INCHES_TO_CENTIMETERS;
}

float kilograms_to_pounds(float kg)
{
    return kg / POUNDS_TO_KILOGRAMS;
}

float celsius_to_fahrenheit(float celsius)
{
    return 1.8f * celsius + 32;
}

float grams_to_pounds(float grams)
{
    return kilograms_to_pounds(grams / 1000.0f);
}


#pragma mark - US to metric

float inches_to_cm(float inches)
{
    return inches * INCHES_TO_CENTIMETERS;
}

float pounds_to_kilograms(float lbs)
{
    return lbs * POUNDS_TO_KILOGRAMS;
}

float fahrenheit_to_celsius(float fahrenheit)
{
    return (fahrenheit - 32.0f)/1.8f;
}

float pounds_to_grams(float pounds)
{
    return pounds_to_kilograms(pounds) * 1000.0f;
}

void getFeetInches(float cm, uint8_t *feet, uint8_t *inches)
{
    float f_inches = cm_to_inches(cm);
    float f_feet = f_inches / 12.0f;
    *feet = (uint8_t)truncf(f_feet);
    float fx_feet = f_feet - *feet;
    *inches = ceil(12.0f * fx_feet);
}