#import "NSString+BinaryConversions.h"

@implementation NSString (BinaryConversions)

+ (NSString *)stringWithBinaryRepresentation:(NSInteger)byze bits:(NSInteger)biz width:(NSInteger)strwid {
    NSInteger i, j;
    char *str = (char *)malloc(256 * sizeof(char));
    char *s = str;
    j = strwid - (biz + (biz >> 2)- (biz % 4 ? 0 : 1));
    for (i = 0; i < j; i++)
        *(s++) = ' ';
    i = 0;
    while (--biz >= 0)
    {
		*(s++) = ((byze >> biz) & 1) + '0';
		if (!(biz % 4) && biz)
			*(s++) = ' ';
	}
    *s = '\0';
	NSString *temp = [NSString stringWithUTF8String:str];
	return temp;
}

+ (NSString *)stringWithUnformattedBinaryRepresentation:(size_t)size data:(const void *const)ptr {
    char *str = (char *)malloc(256 * sizeof(char));
    char *s = str;
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;
    
    for (i=size-1;i>=0;i--)
    {
        for (j=7;j>=0;j--)
        {
            byte = b[i] & (1<<j);
            byte >>= j;
            sprintf(s++,"%u",byte);
        }
    }
    *s = '\0';
    NSString *temp = [NSString stringWithUTF8String:str];
	free(str);
	return temp;
}

- (NSInteger)integerFromBinaryString {
    const char *s = [self UTF8String];
	unsigned int i, j = 0;
	while (s && *s && strchr("01", *s))
	{
		i = *s++ - '0';
        j <<= 1;
        j |= (i & 0x01);
    }
	return (NSInteger)j;
}
@end
