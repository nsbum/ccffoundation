#ifndef CCFFoundation_conversions_h
#define CCFFoundation_conversions_h

#import <inttypes.h>

#pragma mark - Metric to US

extern inline float cm_to_inches(float cm);
extern inline float kilograms_to_pounds(float kg);
extern inline float celsius_to_fahrenheit(float celsius);
extern inline float grams_to_pounds(float grams);

#pragma mark - US to metric

extern inline float inches_to_cm(float inches);
extern inline float pounds_to_kilograms(float lbs);
extern inline float fahrenheit_to_celsius(float fahrenheit);
extern inline float pounds_to_grams(float pounds);

extern inline void getFeetInches(float cm, uint8_t *feet, uint8_t *inches);

#endif
