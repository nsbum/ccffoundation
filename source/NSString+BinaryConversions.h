/**
 *   @file NSString+BinaryConversions.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2013-11-01 06:59:53
 *
 *   @note Copyright 2013 Cocoa Factory, LLC.  All rights reserved
 */

#import <inttypes.h>

@interface NSString (BinaryConversions)

+ (NSString *)stringWithBinaryRepresentation:(NSInteger)byze bits:(NSInteger)biz width:(NSInteger)strwid;
+ (NSString *)stringWithUnformattedBinaryRepresentation:(size_t)size data:(void const * const)ptr;

- (NSInteger)integerFromBinaryString;

@end
