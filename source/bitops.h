/**
 *   @file bitops.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2013-10-12 09:01:46
 *
 *   @note Copyright 2013 Cocoa Factory, LLC.  All rights reserved
 */

#include <stdint.h>
#include <stdbool.h>

#pragma mark - Swapping

/**
 *	Swaps a range of bits in an unsigned int
 *
 *	@param	i	Index of the starting bit in the first range
 *	@param	j	Index of the starting bit in the second range
 *	@param	n	The number of bits in the range
 *	@param	b	Bits to swap reside in this parameter
 *
 *	@return	Returns b with the bits swapped as specified
 */
extern unsigned int swap_bits(unsigned int i, unsigned int j, unsigned int n, unsigned int b);

#pragma mark - Reversing bits

/**
 *	Reverse the bits in a byte using 64-bit multiply and modulus division
 *
 *	@param	b	The byte to reverse
 *  
 *  Source:  http://graphics.stanford.edu/~seander/bithacks.html#BitReverseObvious
 *  Notes: The multiply operation creates five separate copies of the 8-bit byte pattern to fan-out into a 64-bit value. The AND operation selects the bits that are in the correct (reversed) positions, relative to each 10-bit groups of bits. The multiply and the AND operations copy the bits from the original byte so they each appear in only one of the 10-bit sets. The reversed positions of the bits from the original byte coincide with their relative positions within any 10-bit set. The last step, which involves modulus division by 2^10 - 1, has the effect of merging together each set of 10 bits (from positions 0-9, 10-19, 20-29, ...) in the 64-bit value. They do not overlap, so the addition steps underlying the modulus division behave like or operations.
 This method was attributed to Rich Schroeppel in the Programming Hacks section of Beeler, M., Gosper, R. W., and Schroeppel, R. HAKMEM. MIT AI Memo 239, Feb. 29, 1972.
 *
 *	@return	Returns b with the bits reversed
 */
extern unsigned char reverse_bits8(unsigned char b);

/**
 *	Reverse the bits in an bytes without using any 64-bit operations
 *
 *  Source: http://graphics.stanford.edu/~seander/bithacks.html#BitReverseObvious
 *  Notes: Make sure you assign or cast the result to an unsigned char to remove garbage in the higher bits. Devised by Sean Anderson, July 13, 2001. Typo spotted and correction supplied by Mike Keith, January 3, 2002
 *
 *	@param	b	The byte to reverse
 *
 *	@return	Returns b with the bits reversed
 */
extern unsigned char reverse_bits8_no64(unsigned char b);

#pragma mark - Circular rotation

/**
 *	Rotate the bits of an unsigned integer in the leftward direction
 *
 *	@param	value	The value to rotate
 *	@param	shift	The number of bits to rotate
 *
 *	@return	The provided value with bits rotated \c shift bits to the left.
 */
extern unsigned int rotl(unsigned int value, int shift);

/**
 *	Rotate the bits of an unsigned integer in the rightward direction
 *
 *	@param	value	The value to rotate
 *	@param	shift	The number of bits to rotate
 *
 *	@return	The provided value with bits rotated \c shift bits to the right.
 */
extern unsigned int rotr(unsigned int value, int shift);

/**
 *	Rotate the bits of an unsigned 32-bit integer in the leftward direction
 *
 *	@param	value	The value to rotate
 *	@param	shift	The number of bits to rotate
 *
 *	@return	The provided value with bits rotated \c shift bits to the left.
 */
extern uint32_t rotl32(uint32_t value, int shift);

/**
 *	Rotate the bits of an unsigned 32-bit integer in the rightward direction
 *
 *	@param	value	The value to rotate
 *	@param	shift	The number of bits to rotate
 *
 *	@return	The provided value with bits rotated \c shift bits to the right.
 */
extern uint32_t rotr32(uint32_t value, int shift);

/**
 *	Rotate the bits of an unsigned 8-bit integer in the leftward direction
 *
 *	@param	value	The value to rotate
 *	@param	shift	The number of bits to rotate
 *
 *	@return	The provided value with bits rotated \c shift bits to the left.
 */
extern uint8_t rotl8(uint8_t value, int shift);

/**
 *	Rotate the bits of an unsigned 8-bit integer in the rightward direction
 *
 *	@param	value	The value to rotate
 *	@param	shift	The number of bits to rotate
 *
 *	@return	The provided value with bits rotated \c shift bits to the right.
 */
extern uint8_t rotr8(uint8_t value, int shift);

#pragma mark - Setting and clearing bits

/**
 *	Sets the given bit in a signed integer
 *
 *	@param	value	A pointer to the integer to set
 *	@param	bit	The index of the bit to be set
 */
extern void sbi(int *value, uint8_t bit);

/**
 *	Clears a specific bit in an integer
 *
 *	@param	value	The value whose bits are to be modified
 *	@param	bit	The index of the bit to be cleared
 *
 *	@return	The provided value with the bit cleared.
 */
extern int cbi(int value, uint8_t bit);

#pragma mark - Testing signedness

/**
 *	Determines whether two 32-bit integers have opposite signs
 *
 *	@param	x	The first integer to compare
 *	@param	y	The other integer to compare
 *
 *	@return	bool true if the integers have the same sign
 */
extern bool signs_opposite32(int32_t x, int32_t y);

/**
 *	Determines whether two 64-bit integers have opposite signs
 *
 *	@param	x	The first integer to compare
 *	@param	y	The other integer to compare
 *
 *	@return	bool true if the integers have the same sign
 */
extern bool signs_opposite64(int64_t x, int64_t y);

#pragma mark - Minimum and maximum

/**
 *	Returns the minimum of two 32-bit integers
 *
 *	@param	x	The first integer to compare
 *	@param	y	The other integer to compare
 *
 *	@return	Returns the minimum of the two integers
 */
extern int32_t min32(int32_t x, int32_t y);

/**
 *	Returns the minimum of two 64-bit integers
 *
 *	@param	x	The first integer to compare
 *	@param	y	The other integer to compare
 *
 *	@return	Returns the minimum of the two integers
 */
extern int64_t min64(int64_t x, int64_t y);

/**
 *	Returns the maximum of two 32-bit integers
 *
 *	@param	x	The first integer to compare
 *	@param	y	The other integer to compare
 *
 *	@return	Returns the maximum of the two integers
 */
extern int32_t max32(int32_t x, int32_t y);

/**
 *	Returns the maximum of two 64-bit integers
 *
 *	@param	x	The first integer to compare
 *	@param	y	The other integer to compare
 *
 *	@return	Returns the maximum of the two integers
 */
extern int64_t max64(int64_t x, int64_t y);

/**
 *	Determines if an unsigned 32-bit integer is a power of 2
 *
 *	@param	v	The integer to test
 *
 *	@return	Boolean true if the provided integer is a power of 2
 */
extern bool is2power32(uint32_t v);

/**
 *	Determines if an unsigned 64-bit integer is a power of 2
 *
 *	@param	v	The integer to test
 *
 *	@return	Boolean true if the provided integer is a power of 2
 */
extern bool is2power64(uint64_t v);

#pragma mark - Testing for ranges of bytes in a word

/**
 *	Determines whether a 32-bit word has any 8-bit byte range of 0
 *
 *	@param	v	The 32-bit word to examine
 *
 *	@return	Boolean true if the provided word has any 8-bit range of 0
 */
extern bool haszero32(uint32_t v);


#pragma mark - Round up power of 2

/**
 *	Rounds a 32-bit integer up to the next highest power of 2
 *
 *	@param	v	The unsigned 32-bit integer to round
 *
 *	@return	The value of \c v rounded up to the next highest power of 2
 */
extern uint32_t round2pwr32(uint32_t v);

/**
 *	Rounds a 64-bit integer up to the next highest power of 2
 *
 *	@param	v	The unsigned 64-bit integer to round
 *
 *	@return	The value of \c v rounded up to the next highest power of 2
 */
extern uint64_t round2pwr64(uint64_t v);

#pragma mark - Count bits set

extern uint32_t bitcount32(uint32_t v);


#define nothing