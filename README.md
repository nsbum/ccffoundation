###About CCFFoundation###

CCFFoundation is a collection of categories on Foundation classes and other utilities that make our lives a little easier at Cocoa Factory.

####At a glance####

- assertions.h / .m _Assertions for use in our code, derived from Omni frameworks_
- CCFUnicodeUtilities.h _Utilities for dealing with unicode, derived from Omni frameworks_
- CCFUtilities.h / .m _Utilities of different sorts, much of which is derived from Omni frameworks.  For example, we have `CCFEncode(coder, var)` and its decoder companion that provides a simple way of handling `NSCoding` conformance._
- macros.h / .m _This is a grab-bag of handy utilities that we use in our code.  Indispensable._
- rand.h / .m _Random number generation.  From a blog post by Mike Ash_
- iOSVersions.h _Some `#define` for iOS versions._
- NSObject+CCFExtensions.h _Master header for several categories on `NSObject`._
- NSArray+CCFExtensions.h / .m _A few handy additions of `NSArray`, like `-firstObject`, `containsObjectIdenticalTo:`.  Some based on Omni Frameworks_
- NSData+Base64.h / .m _Base 64 encoding, of course_
- NSDate+CCFExtensions.h / .m _Heavily based on Omni Frameworks.  All sorts of little adjustments to dates._
- NSDictionary+CCFExtensions.h / .m _Several number access features for convenience.  Also based on Omni Frameworks._
- NSMuableDictionary+CCFExtensions.h / .m _Based again on Omni Frameworks.  Many number setting methods for convenience._
- NSString+CCFExtensions.h / .m _Chopping, stripping, character access, etc._
- NSString+CCFConversions.h / .m _Converting strings to numbers, including hex.  Lots from Omni Frameworks_

####License####

This is open-source.  Just use it.  Most of this is adapted from Omni Frameworks.

####Usage####

You should use this with CocoaPods, the iOS/MacOS dependency management system.  To pull a specific tag from the repository, your Podfile should contain a line like:

    pod 'CCFFoundation', :git => 'https://nsbum@bitbucket.org/nsbum/ccffoundation.git', :tag => '2.0.4'
    
There are two targets - a static library for iOS and a framework for MacOS